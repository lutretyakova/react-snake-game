import { 
        HEIGHT, WIDTH,
        RIGHT, 
        LEFT,
        TOP,
        BOTTOM
    } from "./constants";

import { REFRESH_BOARD, CHANGE_COURSE_KEY } from './actions';


export default (state = init(), actions) =>{
    switch(actions.type){
        case REFRESH_BOARD:
            return refreshBoard(state);
        case CHANGE_COURSE_KEY:
            return changeCourseWithKey(state, actions.keyCode);
        default:
            return state
    }
}

const init = () => {
    const snakeHead = [
        Math.floor(Math.random() * HEIGHT),
        Math.floor(Math.random() * WIDTH)
    ]                    
  
    const apple = getApple([snakeHead]);

    return {
        snake: [snakeHead],
        course: BOTTOM,
        apple,
        count: 0
    }
}


const getApple = (snake) => {
    let x, y = [null, null];
    
    const hasDublicate = (dy, dx) => (snake.filter(item => (item[0] === dy && item[1] === dx)).length > 0)

    do {
        y = Math.floor(Math.random() * (HEIGHT - 1));
        x = Math.floor(Math.random() * (WIDTH - 1));
    } while (hasDublicate(snake, y, x));
    return [y, x]
}




const refreshBoard = (state) => {
    let snake = state.snake;
    
    let snakeHead = [
        snake[0][0], snake[0][1]
    ];
    switch (state.course) {
        case LEFT:
            snakeHead = [
                snakeHead[0],
                snakeHead[1] - 1 < 0 ? WIDTH - 1 : snakeHead[1] - 1
            ];
            break;
        case TOP:
            snakeHead = [
                snakeHead[0] - 1 < 0 ? HEIGHT - 1 : snakeHead[0] - 1,
                snakeHead[1]
            ];
            break;
        case BOTTOM:
            snakeHead = [
                snakeHead[0] + 1 === HEIGHT ? 0 : snakeHead[0] + 1,
                snakeHead[1]
            ];
            break;
        default:
            snakeHead = [
                snakeHead[0],
                snakeHead[1] + 1 === WIDTH ? 0 : snakeHead[1] + 1
            ];
    }

    snake = [snakeHead].concat(snake);

    let apple = state.apple;
    let count = state.count;
    if(apple[0] === snakeHead[0] && apple[1] === snakeHead[1]){
        apple = getApple(snake);
        count++;
    }
    else{
        snake.pop()
    }

    // пересекать змейку нельзя
    if(snake.length > 1){
        const isCrossing = snake
            .map(item => {
                return snake.filter(dItem => (dItem[0] === item[0] && dItem[1] === item[1])).length
            })
            .filter(item => item > 1)
            .length > 0;

        if(isCrossing){
            return init()
        }
    }

    return {
        ...state,
        snake,
        apple,
        count
    }
}


const changeCourseWithKey = (state, keyCode) =>{
    let course = '';
    switch (keyCode){
        case 37: course = LEFT; break;
        case 38: course = TOP; break;
        case 39: course = RIGHT; break;
        case 40: course = BOTTOM; break;
        default: course = state.course;
    }

    return{
        ...state,
        course
    }
}