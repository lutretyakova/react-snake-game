export const WIDTH = 28;

export const HEIGHT = 18;

export const CELL_WIDTH = 30;

export const SNAKE = 1;

export const APPLE = 2;

export const EMPTY = 0;

export const RIGHT = 'right';

export const LEFT = 'left';

export const TOP = 'top';

export const BOTTOM = 'bottom';