import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';

import './index.css';
import reducer from './reducer';
import SnakeGame from './SnakeGame';
import registerServiceWorker from './registerServiceWorker';

const store = createStore(reducer);

ReactDOM.render(
    <Provider store={ store }>
        <SnakeGame />
    </Provider>, 
    document.getElementById('root')
);
registerServiceWorker();
