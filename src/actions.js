export const REFRESH_BOARD = 'refresh';

export const CHANGE_COURSE_KEY = "keyboad";

export const movingTime = () => ({
    type: REFRESH_BOARD
});

export const changeCourseWithKey = (keyCode) => ({
    type: CHANGE_COURSE_KEY,
    keyCode: keyCode
})