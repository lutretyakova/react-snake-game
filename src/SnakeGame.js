import React from 'react';
import TimerMixin from 'react-timer-mixin';

import { connect } from 'react-redux';
import * as actions from './actions';

import './SnakeGame.css';
import Cell from './modules/Cell';
import { WIDTH, HEIGHT, CELL_WIDTH } from './constants';

const createReactClass = require('create-react-class');

let SnakeGame = createReactClass({
    mixins: [ TimerMixin ],

    componentDidMount: function(){
        const { movingTime } = this.props;

        const refresh = () =>{
            this.setTimeout(() => {
                movingTime();
                refresh();
            }, 200);
        }
        refresh();
        document.addEventListener("keydown", this.handleKeyDown);
    },


    render(){
        const cells = [];
        for(let y = 0; y < HEIGHT; y++){
            for(let x = 0; x < WIDTH; x++){
            cells.push(<Cell key={`cell-${(WIDTH * y) + x}`} y={y} x={x}/>)
            }
        }

        const boardStyle = {
            gridTemplateColumns: `repeat(${WIDTH}, ${CELL_WIDTH}px)`,
            maxWidth: `${ WIDTH * CELL_WIDTH }px`
        }

        const { state } = this.props;

        return (
            <div>
                <div className="top-menu">
                    <header>
                        <h1>SnakeGame</h1>
                        <div className="count">{ state.count }</div>
                    </header>
                </div>
                <div className="board" style={boardStyle}>
                    { cells }
                </div>
                <footer>
                    <a href="https://gitlab.com/lutretyakova/react-snake-game">Ссылка на исходный код</a>
                </footer>
            </div>
        );
    },


    componentWillUnmount: function(){
        document.removeEventListener("keydown", this.handleKeyDown)
    },

    handleKeyDown(e){
        if(e.keyCode >=37 && e.keyCode <= 40){
            const {changeCourseWithKey} = this.props;
            changeCourseWithKey(e.keyCode)
        }
    }

})

export default connect((state) => ({state}), actions )(SnakeGame);
