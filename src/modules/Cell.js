import React, { Component } from 'react';
import { connect } from 'react-redux';
import * as actions from '../actions';
import { CELL_WIDTH } from '../constants';

class Cell extends Component{
    render(){
        const cellStyle = {
            width: `${CELL_WIDTH}px`,
            height: `${CELL_WIDTH}px`
        }

        const { y, x } = this.props;
        const { state } = this.props;

        const display = state.snake.filter(item => (item[0] === y && item[1] === x)).length > 0
                            ? "snake"
                            : state.apple[0] === y && state.apple[1] === x
                                ? "apple"
                                : "empty";

        return(
            <div style={cellStyle} className={`cell ${display}`} >
            </div>
        )
    }
}

export default connect(state => ({ state }), actions)(Cell)